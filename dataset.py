"""
Dataset

Includes all the functions that either preprocess or postprocess data.
"""
import logging
import os
import os.path

import monai.transforms
import numpy as np
import pandas as pd
import tifffile
from sklearn import preprocessing
import torch.nn
import torch.utils.data

import config

logger = logging.getLogger("pytorch.dataset")

# Most of the image transformations in monai.transforms
# assumes the input image is in the channel-first format,
# which has the shape (num_channels, spatial_dim_1[, spatial_dim_2, …]).
TRANSFORMS = {
    "minimal": monai.transforms.Compose(
        [
            monai.transforms.AddChannel(),
            monai.transforms.Resize([224, 224]),
        ]
    ),
    "augmentation": monai.transforms.Compose(
        [
            monai.transforms.AddChannel(),
            # The default will flip over all of the axes of the input array.
            monai.transforms.RandZoom(min_zoom=0.8, max_zoom=1.1, prob=0.5),
            monai.transforms.RandSpatialCrop(224, random_size=False),  # roi_size
            monai.transforms.RandHistogramShift(),
        ]
    ),
}


class Dataset(torch.utils.data.Dataset):
    """Dataset"""

    def __init__(self, transforms):
        """
        :param config:
        """
        self.transforms = transforms
        self.transforms.set_random_state(seed=0)

        self.df = pd.read_csv(os.path.join(config.BUILD_DATA_ROOT, "data.csv"))

        self.le = preprocessing.LabelEncoder()
        self.le.fit(self.df["view"])

    def __len__(self):
        return self.df.shape[0]

    def __getitem__(self, idx):
        image_path = os.path.join(config.BUILD_DATA_ROOT, self.df.at[idx, "tiff"])

        array = tifffile.imread(image_path)
        float_array = array.astype(np.float32)

        return {
            "image": self.transforms(float_array),
            "label": self.le.transform([self.df.at[idx, "view"]])[0],
            "image_path": image_path,
        }

    def plot_samples(self):
        """
        Plotting sample images
        """
        raise NotImplementedError

    def make_gif(self):
        """
        Make a gif from a multiple images
        """
        raise NotImplementedError

    def finalize(self):
        raise NotImplementedError


class TrainDataLoader(torch.utils.data.DataLoader):
    """Train data loader"""

    def __init__(self, batch_size, shuffle=True):
        """Create train data loader instance."""
        ds = Dataset(TRANSFORMS["augmentation"])
        ds.df = ds.df.loc[ds.df["use"] == "training"].reset_index(drop=True)
        self.le = ds.le

        super().__init__(ds, batch_size, shuffle=shuffle)


class ValDataLoader(torch.utils.data.DataLoader):
    """Validation data loader"""

    def __init__(self, batch_size, shuffle=True):
        """Create validation data loader instance."""
        ds = Dataset(TRANSFORMS["minimal"])
        ds.df = ds.df.loc[ds.df["use"] == "validation"].reset_index(drop=True)
        self.le = ds.le

        super().__init__(ds, batch_size, shuffle=shuffle)


class TestDataLoader(torch.utils.data.DataLoader):
    """Test data loader"""

    def __init__(self, batch_size, shuffle=False):
        """Create test data loader instance."""
        ds = Dataset(TRANSFORMS["minimal"])
        ds.df = ds.df.loc[ds.df["use"] == "test"].reset_index(drop=True)
        self.idx2example = ds.df["tiff"].tolist()
        self.le = ds.le

        super().__init__(ds, batch_size, shuffle=shuffle)

    def index_to_example_function(self, index):
        """
        For Comet's Confusion Matrix
        """
        return self.idx2example[index]
