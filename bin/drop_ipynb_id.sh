#!/bin/bash
jupyter nbconvert -y --inplace --clear-output --to notebook $1
sed -i '/"id": "[a-z0-9]*",/d' $1
